<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages']   = array();
 
$autoload['libraries']  = array('Curl','database');

$autoload['drivers'] 	= array();

$autoload['helper'] 	= array('url', 'form','funcoes_helper');

$autoload['config'] 	= array();

$autoload['language']   = array();

$autoload['model']      = array();
