<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] 				 = 'tempo';

// CONSULTA O CEP VIA AJAX

$route['tempo/consultarcep/(:num)'] 		 = 'tempo/consultarcep/$i';

// CONTROLA O STEP-BY-STEP

$route['tempo/mostrartempo/(:num)'] 		 = 'tempo/mostrartempo/$i';
$route['tempo/mostrarendereco/(:num)'] 		 = 'tempo/mostrarendereco/$i';
$route['tempo/cep/(:num)'] 		 			 = 'tempo/mostrarcep/$i';

$route['404_override'] 						 = '';
$route['translate_uri_dashes'] 				 = FALSE;
