<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempo extends CI_Controller {

        public function __construct() {
        parent:: __construct();      
        $this->load->library('Curl'); 
        $this->load->helpers('form');  
        $this->load->model("Tempo_model"); 
    }

	public function index()
	{
		$this->load->view('tempo');		              
	}

  // METODO PARA CONSULTAR O CEP
	public function consultarcep() {

        // PEGA O CEP QUE ESTÁ NO 3º PARAMETRO (QUE FOI ENVIADO VIA AJAX GET)
		    $cep = ($this->uri->segment(3)) ? $this->uri->segment(3) : $cep;		
       
        // RETORNA O JSON, OBTIDO DA CLASSE CURL QUE FICA EM LIBRARIES/CURL.PHP
        // BUSCA O METODO CONSULTA PASSANDO O CEP COMO PARAMETRO.
        $resultado   =  json_decode($this->curl->consulta($cep), true);
		
        if (isset($resultado['erro']) || !is_numeric($cep)) {
        	$resultado['erro'] = '1';
        } else {
        	$resultado['logradouro'];
        }
        $resultado['cep'] =  str_replace("-", "", $cep);
        $this->load->view('endereco', $resultado); 


	}

  // METODO PARA MOSTRAR A VIEW CEP
	public function mostrarcep() {

		$cep = ($this->uri->segment(3)) ? $this->uri->segment(3) : $cep;
		
		$resultado['cep'] = $cep;

        $this->load->view('cep', $resultado); 


	}

  // METODO PARA MOSTRAR A VIEW ENDERECO
	public function mostrarendereco() {
    
    // PEGA O CEP QUE ESTÁ NO 3º PARAMETRO (QUE FOI ENVIADO VIA AJAX GET)
		$cep = ($this->uri->segment(3)) ? $this->uri->segment(3) : $cep;
				
		if (isset($resultado['erro']) || !is_numeric($cep)) {
        	$resultado['erro'] = '1';
        } else {
        	$resultado['logradouro'];
        }
        $resultado['cep'] = $cep;

        $this->load->view('endereco', $resultado); 


	}

  // METODO PARA MOSTRAR A VIEW TEMPO
	public function mostrartempo() {

          // PEGA O CEP QUE ESTÁ NO 3º PARAMETRO (QUE FOI ENVIADO VIA AJAX GET)
	        $cep 		 = ($this->uri->segment(3)) ? $this->uri->segment(3) : $cep;
		
                // RETORNA O JSON, OBTIDO DA CLASSE CURL QUE FICA EM LIBRARIES/CURL.PHP
                // BUSCA O METODO CONSULTA PASSANDO O CEP COMO PARAMETRO.
                $resultado       = json_decode($this->curl->consulta($cep), true);
	
        // SE O RESULTADO DO ARRAY SER IGUAL A 1 OU SE O CEP FOR NUMERICO, RETORNA ERRO.
        if (isset($resultado['erro']) || !is_numeric($cep)) {
        	$resultado['erro'] = '1';
        } else {

          // RETORNA O JSON, OBTIDO DA CLASSE CURL QUE FICA EM LIBRARIES/CURL.PHP
          // BUSCA O METODO CONSULTATEMPO PASSANDO A CIDADE COMO PARAMETRO.
        	$tempoagora  =  json_decode($this->curl->consultatempo($resultado['localidade']), true);
        }

        // PARAMETROS RESULTADO DO JSON CEP

        $tempo['cep']          = soNumero($resultado['cep']);
        $tempo['logradouro']   = $this->input->post('logradouro',true);// input logradouro
        $tempo['cidade']       = $this->input->post('cidade',true);// input cidade
        $tempo['bairro']       = $this->input->post('bairro',true);// input bairro
        $tempo['uf']           = $this->input->post('uf',true);// input uf
        $tempo['numero']       = $this->input->post('numero',true);// input numero
        // PARAMETROS RESULTADO DO JSON TEMPO

        $tempo['temperatura']  = $tempoagora['results']['temp'];
        $tempo['descricao']    = $tempoagora['results']['description'];
        $tempo['umidade']      = $tempoagora['results']['humidity'];
        $tempo['velocidade']   = $tempoagora['results']['wind_speedy'];
        $tempo['img']          = $tempoagora['results']['img_id'];

        // SALVAR NO BANCO DE DADOS, PASSANDO POR ARRAY CAMPOS->VALOR
       
        $ip = $_SERVER['REMOTE_ADDR'];
      

        $fields = array(
              'log_cep' => soNumero($resultado['cep']),
              'log_logradouro' => $tempo['logradouro'],
              'log_numero' => $tempo['numero'],
              'log_bairro' => $tempo['bairro'],
              'log_cidade' => $tempo['cidade'],
              'log_uf' => $tempo['uf'],
              'log_ip' => $ip,
              'log_temperatura' => $tempo['temperatura'],
              'log_umidade' => $tempo['umidade'],
              'log_velocidade' => $tempo['velocidade'],
              'log_descricao' => $tempo['descricao'],
        );
        // ENVIA NO MODEL
        
        $this->Tempo_model->Salvar($fields);
        
        // MOSTRA A PREVISÃO DO TEMPO, PEGANDO OS PARAMETROS $TEMPO
        $this->load->view('previsao', $tempo); 


	}
        

}
