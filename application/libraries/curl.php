<?php

class Curl {
 
 	private $url_ws	   = 'http://viacep.com.br/ws';
 	private $url_tempo = 'https://api.hgbrasil.com/weather/?format=json';

 
    public function consulta($cep) {
 
        $this->url_ws = $this->url_ws . '/' . $cep . '/json/';
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $this->url_ws);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        $resultado = curl_exec($ch);
 
        if (curl_error($ch)) {
            echo 'Erro:' . curl_error($ch);
            return false;
        }
        return $resultado;
 		
        curl_close($ch);
 
    }
    

    public function consultatempo($cidade) {
 		

        $this->url_tempo = $this->url_tempo . '&city_name=' .rawurlencode($cidade) . '&key=fe21b002';
 	
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $this->url_tempo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $resultado = curl_exec($ch);
 
        if (curl_error($ch)) {
            echo 'Erro:' . curl_error($ch);
            return false;
        }
        return $resultado;
 		
        curl_close($ch);
 
    }
 
}

?>