
<h3>Digite seu CEP e obtenha as informações de localização e previsão de tempo para a sua região.</h3>

		<p></p>
	
		<div id="container">
			<label class="col-md-2 row">CEP
				<div class="ui input">
				<input value="<?php if (isset($cep) != "") { echo $cep;} else { echo '06404326'; } ?>" id="cep" type="text" autofocus maxlength="8" value="" class="form-control">
				</div>
			</label>

			<div class="col-md-12" style="text-align: right;">
				<button onclick="consultarcep();" id="consultar-cep" class="ui primary right labeled icon button">
				  <i class="right arrow icon"></i>
				  Próximo
				</button>
			</div>

		</div>