<?php
	
	if (isset($erro) != "") {
		
		echo "<script>alert('CEP inválido');location.reload();</script>";
	} else {

 ?>
<h3>Complete seu endereço</h3>
<div class="row">
	<div class="ui form">
	<label class="col-md-2">CEP

				<input value="<?php if (isset($cep) != "") { echo $cep;} ?>" id="cep" type="text" value="" maxlength="8" class="form-control">
	</label>

	<label class="col-md-4">Logradouro
		<input type="text" value="<?php echo $logradouro; ?>" class="form-control" id="logradouro">
	</label>
	
	<label class="col-md-2">Número
		<input type="number" autofocus="" class="form-control" id="numero">
	</label>
	
	<label class="col-md-4">Bairro
		<input type="text" value="<?php echo $bairro; ?>"  class="form-control" id="bairro">
	</label>
	
	<label class="col-md-6">Cidade
		<input type="text" value="<?php echo $localidade; ?>"  class="form-control" id="cidade">
	</label>

	<label class="col-md-2">Estado
		<input type="text" value="<?php echo $uf; ?>"  class="form-control" id="uf">
	</label>

	<div class="col-md-12" style="text-align: right;">
				<button onclick="consultartempo();" id="consultar-previsao" class="ui primary right labeled icon button">
				  <i class="right arrow icon"></i>
				  Próximo
				</button>
			</div>
		</form>
</div>	
<?php } ?>		
