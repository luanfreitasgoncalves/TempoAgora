

<h3>Previsão do Tempo para <b><?php echo $cidade; ?></b></h3>
<hr>

</br>

<div class="ui centered card">
  <div class="prev"><span class="temp"><?php echo $temperatura; ?>º</span></div>
  <div class="image">
    <img src="<?= base_url(); ?>assets/imagens/<?php echo $img; ?>.png" class="imagem-do-tempo">
  </div>
  <div class="content">
    <a class="header"><?php echo $descricao; ?></a>
  </div>
  <div class="extra content">
    <span class="left floated">
      <i class=""></i>
      Umidade: <b><?php echo $umidade; ?>%</b>
    </span>
    <span class="right floated">
      <i class=""></i>
      Vento: <b><?php echo $velocidade; ?></b>
    </span>
  </div>
</div>

<div class="col-md-12" style="text-align: center;">
	<button onclick="javascript:location.reload();" class="ui primary button">
	  Tentar novamente!
	</button>
</div>


