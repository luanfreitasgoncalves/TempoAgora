    

<script type="text/javascript" src="<?php echo base_url('lib/jQuery/jquery-3.1.1.min.js'); ?>"></script>

<script src="<?php echo base_url('lib/Semantic-UI/semantic.min.js'); ?>" 
 ></script>  

<script type="text/javascript" src="<?php echo base_url('lib/bootstrap/js/bootstrap.min.js'); ?>"></script>
    
<script>
	
		 function consultarcep () {
	  	 
	    	$('#retorno').html('<div class="ui active centered inline loader"></div>');

			   $.ajax({
	                url: '<?= base_url(); ?>index.php/tempo/consultarcep/'+$("#cep").val(),
	                type: "POST",
	                success: function(data){
	                	$("#body").html(data);
	                	$(".cep").attr('class','step cep');
						$(".cep").attr('class','step cep');	                	
	                    $(".endereco").attr('class','active step endereco');
	                    $('#retorno').html('');
	                }        
	           });
		  }

		  function sbs_cep () {	  	 
	    
			   $.ajax({
	                url: '<?= base_url(); ?>index.php/tempo/mostrarcep/'+$("#cep").val(),
	                type: "POST",
	                success: function(data){
	                	$("#body").html(data);

	                	$(".cep").attr('class','active step cep');
	                    $(".endereco").attr('class',' step endereco');	                    
	                    $(".tempo").attr('class','step tempo');

	                    $(".endereco").attr('onclick','');
	                    $(".tempo").attr('onclick','');
	                }        
	           });
		  }

		   function sbs_endereco () {	  	 
	    
			   $.ajax({
	                url: '<?= base_url(); ?>index.php/tempo/mostrarendereco/'+$("#global_cep").val(),
	                type: "POST",
	                success: function(data){
	                	$("#body").html(data);
	                	$(".cep").attr('class','step cep');
	                    $(".endereco").attr('class','active step endereco');
	                    $(".tempo").attr('class','step tempo');
	                }        
	           });
		  }
 
	  	   function consultartempo () {
	    
	    	$('#retorno').html('<div class="ui active centered inline loader"></div>');
	    		    	
			   $.ajax({
	                url: '<?= base_url(); ?>index.php/tempo/mostrartempo/'+$("#cep").val(),
	                type: "POST",
	                data:	                
	                '&logradouro='+$("#logradouro").val()+
	                '&numero='+$("#numero").val()+
	                '&bairro='+$("#bairro").val()+
	                '&cidade='+$("#cidade").val()+
	                '&uf='+$("#uf").val(),
	                
	                success: function(data){
	                	$("#body").html(data);
	                	$(".cep").attr('class','disabled step cep');
	                    $(".endereco").attr('class','disabled step endereco');
	                    $(".tempo").attr('class','active step tempo');
	                    $('#retorno').html('');
	                }        
	           });
		  }
 

</script>