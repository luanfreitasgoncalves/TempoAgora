<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Tempo Agora!</title>
	 
	<?php $this->load->view("css"); ?>
	
</head>
<body background="">
<style>
	body {
	   position: static;
	}
    body:after {
    content : "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    background-image: url('<?= base_url(); ?>/lib/css/fundo.jpg') ; 
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    wbackground-size: cover;
    width: 100%;
    height: 100%;
    opacity : 0.2;
    z-index: -1;
}
</style>

<div class="container" class="col-md-12">
		<h1>Bem vindo ao Tempo Agora!</h1>
		<div class="ui unstackable steps col-md-12">
		  <div onclick="sbs_cep();" class="active step cep">
		    <i class="text cursor icon"></i>
		    <div class="content">
		      <div class="title">CEP</div>
		      <div class="description">Digite o seu CEP</div>
		    </div>
		  </div>

		  <div onclick="sbs_endereco();" class="disabled step endereco">
		    <i class="disabled map icon"></i>
		    <div class="content">
		      <div class="title">Endereço</div>
		      <div class="description">Complete seu Endereço</div>
		    </div>
		  </div>

		  <div class="disabled step tempo">
		    <i class="cloud icon"></i>
		    <div class="content">
		      <div class="title">Previsão do Tempo</div>
		      <div class="description">Obtenha previsão do Tempo</div>
		    </div>
		  </div>
		</div>

	<div id="body" class="col-md-12 row" >
		<?php $this->load->view('cep'); ?>
	</div>


</div>

<div id="retorno"></div>
<?php $this->load->view("scripts"); ?>

<input type="text" id="global_cep" style="display: none;" value="">

</body>
</html>