###################
Tempo Agora
###################

Esse é um sistema desenvolvido para mostrar a previsão do tempo com base no CEP informado.

###################
Objetivo
###################

O Usuário irá informar o CEP, completar as informações de localização e obter a previsão do tempo para região.

###################
Frameworks utilizados no projeto:
###################

-  `CodeIgniter <https://codeigniter.com/docs>`_
-  `Bootstrap <http://getbootstrap.com>`_
-  `JQuery <http://jquery.org>`_
-  `Semantic UI <https://github.com/Semantic-Org/Semantic-UI>`_

###################
Apis consumidas
###################

-  `HgBrasil <https://hgbrasil.com/status/weather>`_
-  `ViaCEP <https://viacep.com.br>`_

Obs: Para consumir as Apis foi utilizado o Próprio Curl do PHP devido a simplicidade do negócio.

###################
Banco de Dados
###################

Foi utilizado o mysqli do codeigniter.

*******************
Requisitos do Sistema
*******************

Se você estiver em uma máquina local, será necessário instalar o servidor `Xampp <https://www.apachefriends.org/pt_br/download.html>`_ ou `Wamp <http://www.wampserver.com/en/>`_


************
Instalação
************

-  Faça o download do repositório, crie uma pasta chamada 'tempoagora' dentro da pasta htdocs (Xampp) ou www (Wamp) em sua máquina. Deverá se parecer como esse exemplo: C:/wamp/www/[tempoagora] ou C:/xampp/htdocs/[tempoagora].
-  Importe todos os arquivos contidos na pasta baixada para a nova pasta criada.
-  Inicie o Servidor MySQl e Apache
-  Crie um Banco de Dados chamado tempoagora em root.
-  Importe o arquivo sql.sql que está na pasta do sistema
-  Navegue em http://127.0.0.1/tempoagora

###################
`DEMO <http://apetite.pe.hu/tempoagora/>`_
###################
