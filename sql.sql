-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 01-Dez-2017 às 19:42
-- Versão do servidor: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tempoagora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,  
  `log_ip` varchar(25) NOT NULL,
  `log_temperatura` float(10,2) DEFAULT NULL,
  `log_umidade` varchar(20) DEFAULT NULL,
  `log_velocidade` varchar(20) DEFAULT NULL,
  `log_descricao` varchar(60) DEFAULT NULL,
  `log_cep` int(8) NOT NULL,
  `log_logradouro` varchar(120) DEFAULT NULL,
  `log_numero` varchar(5) DEFAULT NULL,
  `log_bairro` varchar(50) DEFAULT NULL,
  `log_uf` varchar(2) DEFAULT NULL,
  `log_cidade` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idlog`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
